package ch3.part1;

/**
 * BinarySearchTree 的测试类
 *
 * @author liuwanxiang
 * @version 2019/05/17
 */
public class BinarySearchTreeTests {

    public static void main(String[] args) {

        BinarySearchTree tree = new BinarySearchTree();

        BinarySearchTree.TreeNode rootNode = tree.insert(7);
        tree.insert(6);
        tree.insert(5);
        tree.insert(9);
        tree.insert(8);
        tree.insert(10);
        tree.insert(11);
        tree.insert(10);
        tree.insert(4);
        tree.insert(3);

        BinarySearchTree.toString(tree.delete(7));
        BinarySearchTree.toString(tree.delete(10));

        BinarySearchTree.toString(rootNode);

        System.out.print("\n====================\n");

        BinarySearchTree.toString(tree.search(8));

        System.out.print("\n====================\n");

        BinarySearchTree.TreeNode node6 = tree.search(6);
        System.out.println(BinarySearchTree.getMaxDepth(node6));
        System.out.println(BinarySearchTree.getMinDepth(node6));
        System.out.println(BinarySearchTree.isBalanced(node6));

        System.out.println(tree.maxDepth());
        System.out.println(tree.minDepth());
        System.out.println(tree.isBalanced());

        //--------------------------------------------

        BinarySearchTree tree5 = new BinarySearchTree();
        tree5.insert(5);
        tree5.insert(6);
        tree5.insert(4);
        tree5.insert(3);
        System.out.println(tree5.isBalanced());

    }

}
