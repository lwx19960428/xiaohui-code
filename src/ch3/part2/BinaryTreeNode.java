package ch3.part2;

import java.util.List;

/**
 * 二叉树的节点 Node 对象
 *
 * @author liuwanxiang
 * @version 2019/05/16
 */
public class BinaryTreeNode {

    Integer data;
    BinaryTreeNode leftChild;
    BinaryTreeNode rightChild;

    public BinaryTreeNode(Integer data) {
        this.data = data;
    }

    /**
     * 重写 toString() 方法，方便打印
     *
     * @return
     */
    @Override
    public String toString() {
        return data == null ? null : data.toString();
    }


    /**
     * 根据 List 数组 or 链表对象创建二叉树
     *
     * @param list
     * @return
     */
    public static BinaryTreeNode createBinaryTree(List<Integer> list) {
        BinaryTreeNode node = null;
        if (list == null || list.isEmpty()) {
            return null;
        }
        Integer data = list.remove(0);
        if (data != null) {
            node = new BinaryTreeNode(data);
            node.leftChild = createBinaryTree(list);
            node.rightChild = createBinaryTree(list);
        }
        return node;
    }

}
