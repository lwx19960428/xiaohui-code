package other;

/**
 * 斐波那契数列数列问题
 *
 * @author liuwanxiang
 * @version 2019/06/18
 */
public class FibonacciSequence {

    /**
     * 案例一：n节台阶，每步可1可2，问有多少中解法？时间复杂度及空间复杂度各为多少？
     * 递归实现
     * 时间复杂度：O(n) = 2 ^ n
     * 空间复杂度：O(n) = n
     *
     * @param n 多少级台阶
     * @return 走法
     */
    private static int stepV1(int n) {
        if (n == 1 || n == 2) {
            return n;
        }
        return stepV1(n - 1) + stepV1(n - 2);
    }

    /**
     * 非递归实现
     * 时间复杂度：O(n) = n
     * 空间复杂度：O(n) = 1
     *
     * @param n 多少级台阶
     * @return 走法
     */
    private static int stepV2(int n) {
        if (n == 1 || n == 2) {
            return n;
        }
        int n1 = 1;
        int n2 = 1;
        for (int i = 2; i < n; i++) {
            n2 = n1 + n2;
            n1 = n2 - n1;
        }
        return n1 + n2;
    }

    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(stepV1(i));
            System.out.println(stepV2(i));
        }
    }

}
