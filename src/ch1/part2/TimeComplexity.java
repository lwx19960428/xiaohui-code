package ch1.part2;

/**
 * 第一章第二小节《以吃面包问题演示时间复杂度》
 *
 * @author liuwanxiang
 * @version 2019/05/13
 */
public class TimeComplexity {

    /**
     * 线性关系 O(n)=n
     *
     * @param length
     */
    static void eat1(int length) {
        for (int i = 0; i < length; i++) {
            System.out.println("等待1min");
            System.out.println("等待1min");
            System.out.println("吃面包1cm");
        }
    }

    /**
     * 对数关系 O(n)=logn
     *
     * @param length
     */
    static void eat2(int length) {
        for (int i = length; i > 1; i /= 2) {
            System.out.println("等待1min");
            System.out.println("等待1min");
            System.out.println("等待1min");
            System.out.println("等待1min");
            System.out.println("吃一半面包");
        }
    }

    /**
     * 常量级 O(n)=1
     *
     * @param length
     */
    static void eat3(int length) {
        System.out.println("等待1min");
        System.out.println("吃掉一个鸡腿");
    }

    /**
     * 指数级 O(n)=n^2
     *
     * @param length
     */
    static void eat4(int length) {
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < i; j++) {
                System.out.println("等待1min");
            }
            System.out.println("吃面包1cm");
        }
    }

}
