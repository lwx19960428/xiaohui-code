package ch4.part2;

import java.util.Arrays;

/**
 * 冒泡排序
 * 平均时间复杂度：O(n)=n^2
 *
 * @author liuwanxiang
 * @version 2019/05/17
 */
public class BubbleSort {

    public static void main(String[] args) {
        int[] array = new int[]{5, 4, 3, 2, 1};
        sort1(array);
        System.out.println(Arrays.toString(array));

        array = new int[]{5, 4, 3, 2, 1};
        sort2(array);
        System.out.println(Arrays.toString(array));

        array = new int[]{5, 4, 3, 2, 1};
        sort3(array);
        System.out.println(Arrays.toString(array));

        array = new int[]{5, 4, 3, 2, 1};
        sort4(array);
        System.out.println(Arrays.toString(array));
    }

    /**
     * 时间复杂度：O(n)=n^2
     * <p>
     * 思路：在小区间数组内，各个值依次与第一个元素比较。
     * 每次内循环完毕，最小(大)值总是为第一个元素。
     */
    private static void sort1(int[] array) {
        int tempData;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    tempData = array[i];
                    array[i] = array[j];
                    array[j] = tempData;
                }
            }
            System.out.println(Arrays.toString(array));
        }
    }

    /**
     * 最基础的冒泡排序算法，从小到大排序。
     * 时间复杂度：O(n)=n^2
     * <p>
     * 思路：相邻两个值做比较，较大值向后移动。
     */
    private static void sort2(int[] array) {
        int tempData;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    tempData = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tempData;
                }
            }
        }
    }

    /**
     * 在sort2的基础上，增加了对提前有序的判断
     */
    private static void sort3(int[] array) {
        int tempData;
        boolean isSorted;
        for (int i = 0; i < array.length - 1; i++) {
            isSorted = true;
            // 运行完一次内部循环后，没有进行过交换，
            // 意味着数组已经有序，无需再进行后续轮次的遍历。
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    tempData = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tempData;
                    isSorted = false;
                }
            }
            System.out.println(Arrays.toString(array));
            if (isSorted) {
                break;
            }
        }
    }

    /**
     * 在sort3的基础上，增加了对有序边界的判断
     */
    private static void sort4(int[] array) {
        int tempData;
        boolean isSorted;
        int lastExchangeIndex = 0;
        int sortBorder = array.length - 1;
        for (int i = 0; i < array.length - 1; i++) {
            isSorted = true;
            // isSorted: 运行完一次内部循环后，没有进行过交换，意味着数组已经有序，无需再进行后续轮次的遍历。
            // lastExchangeIndex: 记录本次循环中最后一个交换的元素，并作为下次循环的最大边界。
            for (int j = 0; j < sortBorder; j++) {
                if (array[j] > array[j + 1]) {
                    tempData = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tempData;
                    isSorted = false;
                    lastExchangeIndex = j;
                }
            }
            sortBorder = lastExchangeIndex;
            if (isSorted) {
                break;
            }
        }
    }

}
