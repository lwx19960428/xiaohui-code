package ch4.part3;

import java.util.Arrays;

/**
 * 双边循环排序，不稳定排序
 * 平均时间复杂度：O(n) = nlogn
 * 最坏时间复杂度：O(n) = n ^ 2
 * 平均空间复杂度：O(n) = logn
 *
 * @author liuwanxiang
 * @version 2019/06/13
 */
public class BothCycleQuickSort {

    public static void main(String[] args) {
        int[] array = {1, 2, 5, 4, 3, 9, 8, 6, 7, 0};
        // int[] array = {4, 7, 6, 5, 3, 2, 8, 1};
        sort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    /**
     * QuickSort的递归方法体
     * --------------------------------------------------------------------------------------------
     * n      => s e array(before)                  => array(after)                   => pivotIndex
     * --------------------------------------------------------------------------------------------
     * 1      => 0 9 [1, 2, 5, 4, 3, 9, 8, 6, 7, 0] => [0, 1, 5, 4, 3, 9, 8, 6, 7, 2] => 1
     *  2     => 0 0 [0, 1, 5, 4, 3, 9, 8, 6, 7, 2] => return;
     *  3     => 2 9 [0, 1, 5, 4, 3, 9, 8, 6, 7, 2] => [0, 1, 2, 4, 3, 5, 8, 6, 7, 9] => 5
     *   4    => 2 4 [0, 1, 2, 4, 3, 5, 8, 6, 7, 9] => [0, 1, 2, 4, 3, 5, 8, 6, 7, 9] => 2
     *    5   => 2 1 [0, 1, 2, 4, 3, 5, 8, 6, 7, 9] => return;
     *    6   => 3 4 [0, 1, 2, 4, 3, 5, 8, 6, 7, 9] => [0, 1, 2, 3, 4, 5, 8, 6, 7, 9] => 4
     *     7  => 4 3 [0, 1, 2, 3, 4, 5, 8, 6, 7, 9] => return;
     *     8  => 5 4 [0, 1, 2, 3, 4, 5, 8, 6, 7, 9] => return;
     *   9    => 6 9 [0, 1, 2, 3, 4, 5, 8, 6, 7, 9] => [0, 1, 2, 3, 4, 5, 7, 6, 8, 9] => 8
     *    10  => 6 7 [0, 1, 2, 3, 4, 5, 7, 6, 8, 9] => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] => 7
     *     11 => 6 6 [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] => return;
     *     12 => 8 7 [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] => return;
     *    13  => 9 9 [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] => return;
     * --------------------------------------------------------------------------------------------
     *
     * @param array      数组
     * @param startIndex index开始的下标
     * @param endIndex   index结束的下标
     */
    private static void sort(int[] array, int startIndex, int endIndex) {

        String oldArray = Arrays.toString(array);
        if (startIndex >= endIndex) {
            System.out.printf("%s %s %s => return;\n", startIndex, endIndex, oldArray);
            return;
        }

        // 取得基准元素位置
        int pivotIndex = partition(array, startIndex, endIndex);
        String newArray = Arrays.toString(array);
        System.out.printf("%s %s %s => %s => %s\n", startIndex, endIndex, oldArray, newArray, pivotIndex);

        // 基于基准元素，对左右侧的数据进行排序
        sort(array, startIndex, pivotIndex - 1);
        sort(array, pivotIndex + 1, endIndex);
    }

    /**
     * 同时对左右两侧进行对比，小值放在左侧，大值放在右侧
     *
     * @param array      数组
     * @param startIndex index开始的下标
     * @param endIndex   index结束的下标
     * @return 基准值交换之后的位置
     */
    private static int partition(int[] array, int startIndex, int endIndex) {
        int pivot = array[startIndex];
        int left = startIndex;
        int right = endIndex;

        while (left != right) {
            // 如果右侧元素比基准值小，即停下循环，等待交换元素
            while (left < right && array[right] > pivot) {
                right--;
            }
            // 如果左侧元素比基准值大，即停下循环，等待交换元素
            while (left < right && array[left] <= pivot) {
                left++;
            }
            // 两个待交换元素进行交换
            if (left < right){
                int tempData = array[left];
                array[left] = array[right];
                array[right] = tempData;
            }
        }

        // 将左侧的最后一个值与基准值进行位置交换
        array[startIndex] = array[left];
        array[left] = pivot;

        return left;
    }

}
